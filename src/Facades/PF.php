<?php 
namespace KDA\PF\Facades;

use Illuminate\Support\Facades\Facade;


class PF extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'pf_checkout_client'; }
}