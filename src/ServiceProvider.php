<?php

namespace KDA\PF;


use KDA\Laravel\PackageServiceProvider;
use PostFinanceCheckout\Sdk\ApiClient;


class ServiceProvider extends PackageServiceProvider
{

   

    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }


    public function register(){
        parent::register();

        $this->app->singleton('pf_checkout_client', function ($app) {
            return new ApiClient(env('PF_CHECKOUT_UID'),env('PF_CHECKOUT_SECRET'));
        });


    }

    public function boot(){
        parent::boot();
      
    }


   

}
